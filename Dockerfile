ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

ARG PGDATA=/var/lib/postgresql/data

ENV PGDATA=${PGDATA}

USER 0

COPY signatures/RPM-GPG-KEY-PGDG-14 \
    postgresql14-server.rpm \
    postgresql14.rpm \
    postgresql14-libs.rpm \
    postgresql14-contrib.rpm \
    /tmp/

COPY scripts/docker-entrypoint.sh /usr/local/bin/
RUN chmod 755 /usr/local/bin/docker-entrypoint.sh

RUN rpm --import /tmp/RPM-GPG-KEY-PGDG-14 && \
    dnf install -y glibc-langpack-en /tmp/postgresql14-server.rpm /tmp/postgresql14.rpm /tmp/postgresql14-libs.rpm /tmp/postgresql14-contrib.rpm && \
    dnf clean all && \
    rm -rf /var/cache/dnf && \
    rm -rf /usr/share/doc/perl-IO-Socket-SSL/certs/* && \
    rm -rf /usr/share/doc/perl-IO-Socket-SSL/example && \
    rm -rf /usr/share/doc/perl-Net-SSLeay/examples && \
    chmod +x /usr/local/bin/docker-entrypoint.sh &&\
    chmod o-w /usr/local/bin/docker-entrypoint.sh &&\
    chmod o-w /tmp/RPM-GPG-KEY-PGDG-14

RUN mkdir /docker-entrypoint-initdb.d && \
    chown postgres:postgres /docker-entrypoint-initdb.d && \
    chmod 755 /docker-entrypoint-initdb.d && \
    mkdir -p "$PGDATA" && \
    chown -R postgres:postgres "$PGDATA" && \
    chmod 775 "$PGDATA" && \
    sed -ri s/"#?(listen_addresses)\s*=\s*\S+.*"/"listen_addresses = '*'"/ /usr/pgsql-14/share/postgresql.conf.sample && \
    grep -F "listen_addresses = '*'" /usr/pgsql-14/share/postgresql.conf.sample

# commented out due to CrashLoopBackoff in a cluster without Docker (issue: https://repo1.dso.mil/dsop/opensource/postgres/postgresql12/-/issues/5)
# VOLUME /var/lib/postgresql/data

USER postgres

ENV PGDATA=${PGDATA}
ENV PATH $PATH:/usr/pgsql-14/bin
ENV LANG en_US.utf8

HEALTHCHECK --interval=5s --timeout=3s CMD /usr/pgsql-14/bin/pg_isready -U postgres

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]

EXPOSE 5432

CMD ["postgres"]
